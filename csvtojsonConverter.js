const fs = require('fs')
const csv = require('csvtojson')
const path = require('path')


const matches_File_Path = './src/data/matches.csv'
const delivery_File_Path = './src/data/deliveries.csv'

const modifyFilePath = filePath => filePath.replace('.csv', '.json')

async function csvtojsonConverter(csvFileName){
    const data = await csv().fromFile(path.join(__dirname, csvFileName))
    
    const outFilePath = modifyFilePath(csvFileName)
    console.log({outFilePath})
    fs.writeFileSync(path.resolve(__dirname, outFilePath), JSON.stringify(data))
}

csvtojsonConverter(matches_File_Path)
csvtojsonConverter(delivery_File_Path)