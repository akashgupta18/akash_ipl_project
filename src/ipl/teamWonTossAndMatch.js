// To calculate the number of times each team won the toss and also won the match.

function teamWonTossAndMatch(matches){
        const result = matches.reduce((result, match) => {
            const winner = match['winner']
            const toss_winner = match['toss_winner']
            if(toss_winner === winner){
                if(result[winner]){
                    result[winner] += 1
                } else {
                    result[winner] = 1
                }

            }
            return result
        }, {})
        
    return result
}

module.exports = teamWonTossAndMatch
