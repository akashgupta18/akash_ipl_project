// To calculate extra runs conceded per team per year in 2016 

function extraRunConcededByEachTeam(deliveries,matches){ 

// 'ids' variable holds all the ids in year 2016
    
    const ids = matches.filter(obj =>obj['season'] === '2016').map(obj=>parseInt(obj.id))

    const delivery_2016 = deliveries.filter(del=>ids.includes(parseInt(del['match_id'])))

    return delivery_2016.reduce((extraRuns, delivery) => {

        const bowling_team = delivery['bowling_team']

        if(bowling_team in extraRuns) {

            extraRuns[bowling_team] += parseInt(delivery['extra_runs'])

        } else {

            extraRuns[bowling_team] = parseInt(delivery['extra_runs'])

        }

        return extraRuns

    }, {})

}

module.exports = extraRunConcededByEachTeam;