// To get the highest number of times one player has been dismissed by another player

function highestTimesPlayerDismissByAnotherPlayer(deliveries) {
// to get the players with number of dismissal times 

    const result = deliveries.reduce((result, delivery) => {

        const player_dismiss = delivery['player_dismissed']

        if(player_dismiss !== ''){
            
            if(result[player_dismiss]){
                result[player_dismiss] += 1
            } else {
                result[player_dismiss] = {}
                result[player_dismiss] = 1
            }
        }
        return result
    }, {})

// To get maximum value of player who maximum number of times he dismissed
    let maxTimes = Math.max(...Object.values(result))

// To get the name of that player who has highest number of times he has been dismissed    
    function getDismissedPlayer(obj, value) {
        return Object.keys(obj).find(key => obj[key] === value)
    }

    return getDismissedPlayer(result, maxTimes)
}

module.exports = highestTimesPlayerDismissByAnotherPlayer