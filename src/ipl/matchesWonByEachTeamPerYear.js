// To calculate matches won by each team in each season

function matchesWonByEachTeamPerYear(matches){

    return matches.reduce((teamsWon, match) => {

        const season = match['season']
        const winner = match['winner']

        if(season in teamsWon) {

            if(winner in teamsWon[season]) {

                teamsWon[season][winner] += 1
            
            } else {
                teamsWon[season][winner] = 1
            }

        } else {
            teamsWon[season] = {}
            teamsWon[season][winner] = 1
        }

        return teamsWon
        
    }, {})

}

module.exports = matchesWonByEachTeamPerYear;
