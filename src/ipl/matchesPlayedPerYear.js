// To calculate total matches played per year

function matchesPlayedPerYear(matches) {

 // 'years' variable holds all the season played in matches   
    const years = matches.map(match => match.season)

    const final_result = years.reduce((final_result, year) => {
        if(final_result[year]){
            final_result[year] += 1
        }else{
            final_result[year] = 1
        }
        return final_result
    }, {})

    return final_result;
}


module.exports = matchesPlayedPerYear;