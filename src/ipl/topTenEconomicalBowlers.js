// To calculate top 10 economical bowlers in 2015

function topTenEconomicalBowlers(deliveries,matches){ 

// 'ids' variable hold all the ids of matches in 2015

const ids = matches.filter(obj => obj['season'] === '2015').map(obj => parseInt(obj.id));

const deliveries_2015 = deliveries.filter(del => ids.includes(parseInt(del['match_id'])));

const totalRunsBalls = deliveries_2015.reduce(getTotalRunsAndBalls, {});

const topTenEconomicalBowler = Object.entries(totalRunsBalls).sort((a, b) => a[1].economyRate - b[1].economyRate).slice(0, 10);


return Object.fromEntries(topTenEconomicalBowler);

}

const getTotalRunsAndBalls = function(totalRunsAndBalls, delivery) {

    const bowler = delivery.bowler;

    if (bowler in totalRunsAndBalls) {

        totalRunsAndBalls[bowler].runConceded += parseInt(delivery['total_runs']);
        totalRunsAndBalls[bowler].bowls += 1;
        totalRunsAndBalls[bowler].economyRate = +((totalRunsAndBalls[bowler].runConceded / (totalRunsAndBalls[bowler].bowls) / 6).toFixed(2));

    } else {

        totalRunsAndBalls[bowler] = {};
        totalRunsAndBalls[bowler].runConceded = parseInt(delivery['total_runs']);
        totalRunsAndBalls[bowler].bowls = 1;
        totalRunsAndBalls[bowler].economyRate = +((totalRunsAndBalls[bowler].runConceded / (totalRunsAndBalls[bowler].bowls) / 6).toFixed(2));
    }

    return totalRunsAndBalls;

}

module.exports = topTenEconomicalBowlers