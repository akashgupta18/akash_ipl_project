const fs = require("fs");
const path = require('path');
const output_File_Path = "../public/output/";

// To define matches/deliveries json file path

const matchFilePath = '../data/matches.json'
const deliveryFilePath = '../data/deliveries.json'

// Import all functions from 'ipl.js'

const {matchesPlayedPerYear, 
       matchesWonByEachTeamPerYear, 
       extraRunConcededByEachTeam,
       topTenEconomicalBowlers,
       teamWonTossAndMatch,
       highestTimesPlayerDismissByAnotherPlayer} = require("./ipl.js")


async function getData() {

    const matches = JSON.parse(fs.readFileSync(path.resolve(__dirname, matchFilePath)))
    const deliveries = JSON.parse(fs.readFileSync(path.resolve(__dirname, deliveryFilePath)))

    let results = iplData(matches, deliveries);

    get_JSON_files(results);
}

getData();


// Making an objects of all functions

function iplData(matches, deliveries) {

    let results = {};

    results.matchesPlayedPerYear = matchesPlayedPerYear(matches);

    results.matchesWonByEachTeamPerYear = matchesWonByEachTeamPerYear(matches);

    results.extraRunConcededByEachTeam = extraRunConcededByEachTeam(deliveries, matches);

    results.topTenEconomicalBowlers = topTenEconomicalBowlers(deliveries, matches);

    results.teamWonTossAndMatch = teamWonTossAndMatch(matches);

    results.highestTimesPlayerDismissByAnotherPlayer = highestTimesPlayerDismissByAnotherPlayer(deliveries);

    return results;
}

// To get JSON data into output folder

function get_JSON_files(results) {

    for (let itr in results) {
        console.log(results[itr]);
        let jsonString = JSON.stringify(results[itr]);
        fs.writeFile(`${output_File_Path}${itr}.json `, jsonString, "utf8", err => {
            if (err) {
                console.error(err);
            }
        });
    }
}

