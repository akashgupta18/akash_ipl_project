/**
 * To find total number of matches played per year
 * 
 * @param {Array} matches of IPL 
 * 
 * @returns {object} total number of matches played per year
 */

function matchesPlayedPerYear(matches) {

    // 'years' variable holds all the season played in matches   
       const years = matches.map(match => match.season)
   
       const matchesPlayed = years.reduce((matchesPlayed, year) => {
           if(matchesPlayed[year]){
        
            matchesPlayed[year] += 1
        
        } else {
          
            matchesPlayed[year] = 1
          
        }
        
        return matchesPlayed
       
    }, {})
   
       return matchesPlayed;
   }

/**
 * To calculate matches won by each team in each season
 * 
 * @param {Array} matches of IPL 
 * 
 * @returns {object} Total number of matches won by each team in each season
 */

function matchesWonByEachTeamPerYear(matches){

    return matches.reduce((teamsWon, match) => {

        const season = match['season']
        const winner = match['winner']

        if(season in teamsWon) {

            if(winner in teamsWon[season]) {

                teamsWon[season][winner] += 1
            
            } else {
                teamsWon[season][winner] = 1
            }

        } else {
            teamsWon[season] = {}
            teamsWon[season][winner] = 1
        }

        return teamsWon
        
    }, {})

}

/**
 * To calculate extra runs conceded per team per year in 2016 
 * 
 * @param {Array} matches of IPL 
 * @param {Array} deliveries of IPL matches
 * @returns {object} Extra runs conceded by each team in each season
 */

function extraRunConcededByEachTeam(deliveries,matches){ 

    // 'ids' variable holds all the ids in year 2016
        
        const ids = matches.filter(obj =>obj['season'] === '2016').map(obj=>parseInt(obj.id))
    
        const delivery_2016 = deliveries.filter(del=>ids.includes(parseInt(del['match_id'])))
    
        return delivery_2016.reduce((extraRuns, delivery) => {
    
            const bowling_team = delivery['bowling_team']
    
            if(bowling_team in extraRuns) {
    
                extraRuns[bowling_team] += parseInt(delivery['extra_runs'])
    
            } else {
    
                extraRuns[bowling_team] = parseInt(delivery['extra_runs'])
    
            }
    
            return extraRuns
    
        }, {})
    
    }


/**
 * To find top ten economical bowlers in year 2015 
 * 
 * @param {Array} matches of IPL 
 * @param {Array} deliveries of IPL matches
 * @returns {object} Top ten economical bowlers in year 2015
 */

function topTenEconomicalBowlers(deliveries,matches){ 

    // 'ids' variable hold all the ids of matches in 2015
    
    const ids = matches.filter(obj => obj['season'] === '2015').map(obj => parseInt(obj.id));
    
    const deliveries_2015 = deliveries.filter(del => ids.includes(parseInt(del['match_id'])));
    
    const totalRunsBalls = deliveries_2015.reduce(getTotalRunsAndBalls, {});
    
    const topTenEconomicalBowler = Object.entries(totalRunsBalls).sort((a, b) => a[1].economyRate - b[1].economyRate).slice(0, 10);
        
    return Object.fromEntries(topTenEconomicalBowler);
    
}
    
const getTotalRunsAndBalls = function(totalRunsAndBalls, delivery) {
    
    const bowler = delivery.bowler;
    
    if (bowler in totalRunsAndBalls) {
    
        totalRunsAndBalls[bowler].runConceded += parseInt(delivery['total_runs']);
        totalRunsAndBalls[bowler].bowls += 1;
        totalRunsAndBalls[bowler].economyRate = +((totalRunsAndBalls[bowler].runConceded / (totalRunsAndBalls[bowler].bowls) / 6).toFixed(2));
    
    } else {
    
        totalRunsAndBalls[bowler] = {};
        totalRunsAndBalls[bowler].runConceded = parseInt(delivery['total_runs']);
        totalRunsAndBalls[bowler].bowls = 1;
        totalRunsAndBalls[bowler].economyRate = +((totalRunsAndBalls[bowler].runConceded / (totalRunsAndBalls[bowler].bowls) / 6).toFixed(2));
    }
    
    return totalRunsAndBalls;    
}
    

/**
 * To calculate the number of times each team won the toss and also won the match.
 * 
 * @param {Array} matches of IPL 
 * 
 * @returns {object} number of times won the toss and match by each team
 */

function teamWonTossAndMatch(matches){

    const wonTossAndMatch = matches.reduce((wonTossAndMatch, match) => {

        const winner = match['winner']
        
        const toss_winner = match['toss_winner']
        
        if(toss_winner === winner){
        
            if(wonTossAndMatch[winner]){
        
                wonTossAndMatch[winner] += 1
        
            } else {
        
                wonTossAndMatch[winner] = 1
        
            }
        }

        return wonTossAndMatch

    }, {})
    
return wonTossAndMatch
}

/**
 * To calculate the highest number of times one player has been dismissed by another player
 * 
 * @param {Array} matches of IPL 
 * 
 * @returns {object} A player with highest number of times dismissed by another player
 */

function highestTimesPlayerDismissByAnotherPlayer(deliveries) {

    // to get the players with number of dismissal times 
    
        const dismissHighestTimes = deliveries.reduce((dismissHighestTimes, delivery) => {
    
            const player_dismiss = delivery['player_dismissed']
    
            if(player_dismiss !== ''){
                
                if(dismissHighestTimes[player_dismiss]){
                    
                    dismissHighestTimes[player_dismiss] += 1
                
                } else {
                
                    dismissHighestTimes[player_dismiss] = {}
                    dismissHighestTimes[player_dismiss] = 1
                
                }
            }

            return dismissHighestTimes
        
        }, {})
    
    // To get maximum value of player who maximum number of times he dismissed
        let maxTimes = Math.max(...Object.values(dismissHighestTimes))
    
    // To get the name of that player who has highest number of times he has been dismissed    
        function getDismissedPlayer(obj, value) {
            return Object.keys(obj).find(key => obj[key] === value)
        }
    
        return getDismissedPlayer(dismissHighestTimes, maxTimes)
}
    
   
module.exports =  {
       matchesPlayedPerYear,
       matchesWonByEachTeamPerYear,
       extraRunConcededByEachTeam,
       topTenEconomicalBowlers,
       teamWonTossAndMatch,
       highestTimesPlayerDismissByAnotherPlayer
}
